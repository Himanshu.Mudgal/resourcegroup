variable "resource_group_name" {
  type        = string
  description = "(Required) Name of Resource Group"
  default = "rg-demo"
}

variable "resource_group_location" {
  type        = string
  description = "(Required) Location where we want to implement code"
  default     = "eastus"
}

variable "lock_level_value" {
  type        = string
  default     = "CanNotDelete"
  description = "(Required) Specifies the Level to be used for this Lock. Possible values are `Empty (no lock)`, `CanNotDelete` and `ReadOnly`. Changing this forces a new resource to be created"
}

variable "tag_map" {
  type        = map(string)
  description = "(Optional) Tags for Resource Group"
  default = {
    Name = "jenkins"
  }
}
